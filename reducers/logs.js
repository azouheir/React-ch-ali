import Immutable from 'immutable'

export default (state = Immutable.List(['10/20/16 at 13:11, Ali Bitar (2263397166)']), action) => {
  switch(action.type) {
    case 'addLog' :
      return state.unshift(action.log)
    case 'deleteLog' :
      return state.filter((log,index) => index !== action.index)
    default:
      return state
  }
}