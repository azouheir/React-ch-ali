import Immutable from 'immutable'

export default (state = Immutable.List([]), action) => {
  switch(action.type) {
    case 'addAccount' :
	  var arr=action.account.split("\n")
	  var len=arr.length
	  var a = state.unshift(arr[len-1])
	  for(var i = 1 ; i < len ; i++){
		  a=a.unshift(arr[len - i - 1])
	  }
      return a
    case 'deleteAccount' :
      return state.filter((account,index) => index !== action.index)
    default:
      return state
  }
}