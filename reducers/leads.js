import Immutable from 'immutable'

export default (state = Immutable.List(['Ali Bitar (2263397166)']), action) => {
  switch(action.type) {
    case 'addLead' :
      return state.unshift(action.lead)
    case 'deleteLead' :
      return state.filter((lead,index) => index !== action.index)
    default:
      return state
  }
}