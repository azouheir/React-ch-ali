import Immutable from 'immutable'

export default (state = Immutable.List(["10/15/16 at 15:10 with Ali Bitar (2263397166)"]), action) => {
  switch(action.type) {
    case 'addAppoint' :
      return state.unshift(action.appoint)
    case 'deleteAppoint' :
      return state.filter((appoint,index) => index !== action.index)
    default:
      return state
  }
}