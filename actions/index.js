export function addTodo(todo){
  return {
    type: 'addTodo',
    todo
  }
}

export function deleteTodo(index){
  return {
    type: 'deleteTodo',
    index
  }
}
export function addLead(lead){
  return {
     type: 'addLead',
     lead
  }
}
export function deleteLead(index){
  return {
     type: 'deleteLead',
     index
  }
}
export function addLog (log){
  return {
     type: 'addLog',
     log
  }
}
export function deleteLog (index){
  return {
     type: 'deleteLog',
     index
  }
}
	
export function addAppoint (appoint){
  return {
     type: 'addAppoint',
     appoint
  }
}
export function deleteAppoint (index){
  return {
     type: 'deleteAppoint',
     index
  }
}
export function addAccount (account){
  return {
     type: 'addAccount',
     account
  }
}
export function deleteAccount (index){
  return {
     type: 'deleteAccount',
     index
  }
}
	