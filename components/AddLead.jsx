import React from 'react'
import { connect } from 'react-redux'
import ReactDOM from 'react-dom'

	
const NewLead = ({onChange}) => (
  <fieldset style={{borderLeft:" 3px blue solid", background :"lightgrey",float : "left", fontSize:"12px"}} onClick={onChange}>
    <legend style={{ background :"lightgrey"}}>Lead</legend>
	<label for="name">Name: </label>
    <input id="name" style={{width: "200px"}}/>
	<br></br>
	<br></br>
	<label for="phone"> Phone Number: (***-***-****)<br></br> </label>
	<input id="phone" style={{width: "100px"}}/>
	<br></br>
	<br></br>
	<button id="B" style={{fontSize:"11px"}} onClick={e =>{
		var n=document.getElementById("name").value
		var p=document.getElementById("phone").value
		var arr3=p.split('-')
		if(isNaN(arr3[0])|| isNaN(arr3[1]) || isNaN(arr3[2]) || p.length!=12){
			alert("Phone Number must be a 10 digit number using this format ***-***-***")
		}
		else{
			document.getElementById("B").id=n+"*"+arr3[0]+arr3[1]+arr3[2]
			if(n != "" && p != ""){
				document.getElementById("name").value = ""
				document.getElementById("phone").value = ""
			}
		}
	
	}}>
		Submit </button>

  </fieldset>
)

export default NewLead
