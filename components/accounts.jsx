import React from 'react'
import { connect } from 'react-redux'

import {deleteAccount } from '../actions'

const Accounts = ({accounts, dispatch}) => (
  <div>
	<br></br><br></br><br></br>	<br></br><br></br>
	<br></br>
	<legend style={{borderRight:" 4px blue solid", float:"right", background: "lightgrey"}}>Accounts</legend>
	<br></br>
	<div style={{borderRight:" 4px blue solid",textAlign: "left",float:"right",background :"lightgrey", fontSize:"13px"}}>
    {accounts.accountsRed.map((account, index) => <p
	 key={index}>{account}<button style={{width: "1em", height: "2em",fontSize: "10px"}} onClick={e => {
      dispatch(deleteAccount(index))
    }}>X</button></p>)}
	</div>
  </div>
)


//accounts is the head
function mapStateToProps(accounts) {
  return {
    accounts
  }
}
export default connect(mapStateToProps)(Accounts)
