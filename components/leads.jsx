import React from 'react'
import { connect } from 'react-redux'

import NewLead from './AddLead'
import {  addLead, deleteLead, addAppoint, addLog, addAccount } from '../actions'

const Leads = ({leads, dispatch}) => (
  <div>
    <NewLead onChange={e => {
	console.log(e.target.id)
	  if(e.target.id!=="name" && e.target.id!=="phone" && e.target.id!="" && e.target.id!="B"){
		  var arr=e.target.id.split('*')
		  if(arr[0]=="" || arr[1]==""){
			  alert("Fields can not be empty.")
		  }
		  else{
		  	  dispatch(addLead(arr[0]+" ("+ arr[1]+")"))
		  }
		  e.target.id="B"
	  }
    }}/>
	<ol style={{borderLeft:" 4px blue solid",
						background :"lightgrey", textAlign: "center",float:"left", fontSize: "11px"}}>
    {leads.leadsRed.map((lead, index) => <li key={index}>
							{lead} 
						<button style={{width: "1em", height: "2em",fontSize: "11px"}} onClick={e => {
      dispatch(deleteLead(index))}}>X</button>
	<button style={{width: "2.5em", height: "2em",fontSize: "11px"}} 
			onClick={e => {
				 var date = prompt("Enter date of Appointment mm/dd/yy")
				 if(date != null){
				 	var time = prompt("Enter time of Appointment 00:00")
				 	if (time != null){
					    var t=time.split(':')
						var d=date.split('/')
					    var dd=Number(d[1])
						var mm=Number(d[0])
						var yy=Number(d[2])
						if(isNaN(t[0]) || isNaN(t[1]) || t[0]>23 || t[1]>59 || t[0]<0 || t[1]<0){
							alert("Invalid Time")
					    }
						else if( isNaN(d[0]) || isNaN(d[1]) || mm < 1 || yy < 0 || dd < 1 || mm > 12 ||
								(mm == 2 && dd >28)||
								(mm < 8 && mm%2 == 0 && dd > 30) || (mm >= 8 && mm%2 == 0 && dd > 31) ||
								(mm < 8 && mm%2 == 1 && dd > 31) || (mm >= 8 && mm%2 == 1 && dd > 30)){
								alert("Invalid Date")
						}
						else{
							var t=t.concat(d)
							for(var i = 0; i < 5 ; i++){
								if(Number(t[i])<10){
									t[i]='0'+t[i]
								}
							}
				 			dispatch(addAppoint(t[2]+"/"+t[3]+"/"+t[4]+" "+" at "+t[0]+":"+t[1]+" with "+lead))
						}
					}
				}}}>Sch</button>
	<button style={{width: "2.5em", height: "2em",fontSize: "11px"}} onClick={e => {
				 var date = prompt("Enter date of Call mm/dd/yy")
				 if(date != null){
				 	var time = prompt("Enter time of Call 00:00")
				 	if (time != null){
					    var t=time.split(':')
						var d=date.split('/')
						var dd=Number(d[1])
						var mm=Number(d[0])
						var yy=Number(d[2])
						if(isNaN(t[0]) || isNaN(t[1]) || t[0]>23 || t[1]>59 || t[0]<0 || t[1]<0){
							alert("Invalid Time")
					    }
						else if( isNaN(d[0]) || isNaN(d[1]) || mm < 1 || yy < 0 || dd < 1 || mm > 12 ||
								(mm == 2 && dd >28)||
								(mm < 8 && mm%2 == 0 && dd > 30) || (mm >= 8 && mm%2 == 0 && dd > 31) ||
								(mm < 8 && mm%2 == 1 && dd > 31) || (mm >= 8 && mm%2 == 1 && dd > 30)){
								alert("Invalid Date")
						}
						else{
							var t=t.concat(d)
							for(var i = 0; i < 5 ; i++){
								if(Number(t[i])<10){
									t[i]='0'+t[i]
								}
							}
				 			dispatch(addLog(t[2]+"/"+t[3]+"/"+t[4]+" "+" at "+t[0]+":"+t[1]+", "+lead))
						}
					}
				}}}>Log</button>
	<button style={{width: "2.5em", height: "2em",fontSize: "11px"}} onClick={e => {
				var a=confirm("Create an account for "+lead.split("(")[0]+"?")					 	
				if(a == true){
					var acc=lead+"\n Appointments: \n"
					var ls=[]
					var as=[]
					leads.logsRed.map((log, index)=>{
							var l=log.split(",")
							if(l[1].substring(1)==lead){
								ls=ls.concat(l[0])
							}										
					})
					leads.appointsRed.map((log, index)=>{
							var l=log.split(" with ")
							if(l[1]==lead){
								as=as.concat(l[0])
							}										
					})
					for(var i = 0 ; i < as.length ; i++){
						acc = acc + as[i]+"\n"
					}
					acc=acc+"Logs: \n"
					for(var i = 0 ; i < ls.length ; i++){
						acc = acc + ls[i]+"\n"
					}
					dispatch(addAccount(acc))
				}
				}}>Acc</button>
				</li>)}
	</ol>
  </div>

)


//leads is the head
function mapStateToProps(leads) {
  return {
    leads
  }
}
export default connect(mapStateToProps)(Leads)
