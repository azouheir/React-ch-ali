import React from 'react'
import { connect } from 'react-redux'

import NewLog from './NewLog'
import {  addLog, deleteLog } from '../actions'

const Logs = ({logs, dispatch}) => (
  <div>
	<br></br><br></br><br></br><br></br><br></br>
    <NewLog onChange={e => {
	console.log(e.target.id)
	  if(e.target.id!=="timelog" && e.target.id!=="phonelog" 
		 && e.target.id !== "datelog" && e.target.id !=="namelog" && e.target.id!="" && e.target.id!= "C"){
		  var arr=e.target.id.split('*')
		  if(arr[0]=="" || arr[1]=="" || arr[2]==""|| arr[3] ==""){
			  alert("Fields can not be empty.")
		  }
		  else{
			  var t=arr[2].split(':')
			  var d=arr[3].split('/')
			  var t=t.concat(d)
			  for(var i = 0; i < 5 ; i++){
			  	if(Number(t[i])<10){
					t[i]='0'+t[i]
				}
			  }
		  	  dispatch(addLog(t[2]+"/"+t[3]+"/"+t[4]+" "+" at "+t[0]+":"+t[1]+", "+arr[0]+" ("+arr[1]+")"))
		  }
		  e.target.id="C"
	  }
    }}/>
	<ul style={{borderLeft:" 4px blue solid",
						background :"lightgrey", textAlign: "center",float:"left", fontSize: "10px"}}>
    {logs.logsRed.map((log, index) =>  <li
						
						key={index}>{log} 
						<button style={{width: "1em", height: "2em",fontSize: "11px"}}
						onClick={e => {
      dispatch(deleteLog(index))
    }}><small>X</small></button></li>)}
	</ul>
  </div>
)


//logs is the head
function mapStateToProps(logs) {
  return {
    logs
  }
}
export default connect(mapStateToProps)(Logs)
