import React from 'react'
import { connect } from 'react-redux'

const NewTodo = ({onChange}) => (
  <fieldset style={{float :"right", background: "lightgrey"}}>
	<legend style={{background: "lightgrey"}}> Todos  </legend>
    <textarea maxLength={50} cols={10} rows={4} onKeyUp={onChange}>
	</textarea>
	</fieldset>
)

export default NewTodo
