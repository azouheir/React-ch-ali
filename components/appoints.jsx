import React from 'react'
import { connect } from 'react-redux'

import { addAppoint, deleteAppoint} from '../actions'

const Appoints = ({appoints, dispatch}) => (
  <div>
	<legend style={{ borderRight:" 4px blue solid",float:"right", background: "lightgrey"}}>Appointments</legend>
	<br></br>
	<ul style={{borderRight:" 4px blue solid",textAlign: "right",float:"right",background :"lightgrey", fontSize:"10px"}}>
    {appoints.appointsRed.map((appoint, index) => <li
	 key={index}>{appoint} <button style={{width: "1em", height: "2em",fontSize: "11px"}} onClick={e => {
      dispatch(deleteAppoint(index))
    }}>X</button></li>)}
	</ul>
  </div>
)

//Appoints is the head
function mapStateToProps(appoints) {
  return {
    appoints
  }
}
/*function mapStateToProps(store) {
  return {
    store.todosRed
  }
}*/

export default connect(mapStateToProps)(Appoints)

