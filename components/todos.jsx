import React from 'react'
import { connect } from 'react-redux'

//import store from '../store'
import NewTodo from './NewTodo'
import { addTodo, deleteTodo} from '../actions'

const Todos = ({todos, dispatch}) => (
  <div>
    <NewTodo onChange={e => {
      if(e.keyCode == 13){
		if(todos.todosRed.size<6){
        	dispatch(addTodo(e.target.value))
        	e.target.value = ''
		}
		else{
			e.target.value = '(Delete a Todo before adding more)'
		}
      }
    }}/>
	<ul style={{textAlign: "right",float:"right",background :"lightgrey", fontSize:"10px"}}>
    {todos.todosRed.map((todo, index) => <li
	 key={index}>{todo} <button style={{width: "1em", height: "2em",fontSize: "11px"}} onClick={e => {
      dispatch(deleteTodo(index))
    }}>X</button></li>)}
	</ul>
  </div>
)

//todos is the head
function mapStateToProps(todos) {
  return {
    todos
  }
}
/*function mapStateToProps(store) {
  return {
    store.todosRed
  }
}*/

export default connect(mapStateToProps)(Todos)

