import React from 'react'
import { connect } from 'react-redux'
import ReactDOM from 'react-dom'

	
const NewLog = ({onChange}) => (
  <fieldset style={{borderLeft:" 3px blue solid",
	background :"lightgrey",float : "left",fontSize: "12px"}} onClick={onChange}>
    <legend style={{ background :"lightgrey"}}>Log</legend>
	<label for="datelog"> Date: (mm/dd/yy) </label>
    <input id="datelog" style={{width: "50px"}}/>
	<br></br>
	<label for="timelog"> Time: (00:00) </label>
    <input id="timelog" style={{width: "50px"}}/>
	<br></br>
	<label for="namelog" > Name: </label>
    <input id="namelog" style={{width: "200px"}}/>
	<br></br>
	<br></br>
	<label for="phonelog"> Phone Number: (***-***-****)<br></br></label>
	<input id="phonelog" style={{width: "100px"}}/>
	<br></br>
	<br></br>
	<button id="C" style={{fontSize:"11px"}}  onClick={e =>{
		var n=document.getElementById("timelog").value
		var p=document.getElementById("phonelog").value
		var d=document.getElementById("datelog").value
		var x=document.getElementById("namelog").value
		var arr=d.split('/')
		var arr2=n.split(':')
		var arr3=p.split('-')
		var dd=Number(arr[1])
		var mm=Number(arr[0])
		var yy=Number(arr[2])
		if(isNaN(arr3[0])|| isNaN(arr3[1]) || isNaN(arr3[2]) || p.length!=12){
			alert("Phone Number must be a 10 digit number using this format ***-***-***")
		}
		else if(isNaN(arr[0]) || isNaN(arr[1]) || isNaN(arr[2]) || mm < 0 || yy < 0 || dd < 0 || mm > 12 ||
				(mm == 2 && dd >28)||
				(mm < 8 && mm%2 == 0 && dd > 30) || (mm >= 8 && mm%2 == 0 && dd > 31) ||
				(mm < 8 && mm%2 == 1 && dd > 31) || (mm >= 8 && mm%2 == 1 && dd > 30) ) {
			alert("Enter a valid Date")
		}
		else if(isNaN(arr2[0]) || arr2[0]<0 || arr2[0]>23 
				|| isNaN(arr2[1]) || arr2[1]<0 || arr2[1]>59) {
			alert("Enter a valid Time")
		}
		else if(x == ""){
			alert("Fields can not be empty")
		}
		else{
			document.getElementById("C").id=x+"*"+arr3[0]+arr3[1]+arr3[2]+"*"+n+"*"+d
				if(n != "" && p != "" && d != ""){
					document.getElementById("timelog").value = ""
					document.getElementById("phonelog").value = ""
					document.getElementById("datelog").value = ""
					document.getElementById("namelog").value = ""
				}
		}
		
	}}>
		Submit </button>
  </fieldset>
)

export default NewLog
