import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from '../store'
import Todos from './todos'
import Leads from './leads'
import Logs from './logs'
import Appoints from './appoints'
import Accounts from './accounts'
	
const App = () => (
	<div>
	<Todos/>
	<Leads/>
	<br></br>
	<br></br>
	<br></br>
	<br></br>

	<Logs/>
	<Appoints/>
	<Accounts/>
	</div>
)

let reactElement = document.getElementById('react')
render(
  <Provider store={store}>
    <App />
  </Provider>,
  reactElement
)
