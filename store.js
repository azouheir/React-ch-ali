import { createStore, combineReducers } from 'redux';
import todos from './reducers/todos'
import leads from './reducers/leads'
import logs  from './reducers/logs'
import appoints from './reducers/appoints'
import accounts from './reducers/accounts'

	
const reducers = combineReducers({
	todosRed : todos,
	leadsRed : leads,
	logsRed  : logs,
	appointsRed: appoints,
	accountsRed: accounts
	
});
	
//export default createStore(todos)
export default createStore(reducers)